﻿using UnityEngine;
using System.Collections;

public class BlockManager : MonoBehaviour {
	public Transform prefab;
	public float gap= 20;
	public GameObject blockContainer;
	public float offset = 0;
	
	// Use this for initialization
	void Start () {
		StartCoroutine (SpawnBlocks ());
	
	}
	
	// Update is called once per frame
	void Update () {

	}
	private IEnumerator SpawnBlocks()
	{
		while (true) {
				Debug.Log ("foo");
				Create ();
				yield return new WaitForSeconds (6f);
			}
	}

	
	public void Create()
	{
		Transform up = GameObject.Instantiate (prefab) as Transform;
		float height = System.Convert.ToSingle (new System.Random ().NextDouble () * 5.0);
		Debug.Log (height);
		up.Translate (Vector3.up * (height-offset));
		Transform dn = GameObject.Instantiate (prefab) as Transform;
		dn.Translate (Vector3.up * (height - gap - 10.0f-offset));
		up.parent = blockContainer.transform;
		dn.parent = blockContainer.transform;
	}
}
