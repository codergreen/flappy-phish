﻿using UnityEngine;
using System.Collections;

public class BlockScript : MonoBehaviour {
	public static float speed = 2f;
	public Transform prefab;
	// Use this for initialization
	void Start () {
		StartCoroutine (LifeSpan ());
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (new Vector3 (-speed * Time.deltaTime, 0, 0));

	}
	public IEnumerator LifeSpan(){
		yield return new WaitForSeconds (10);

	}

	
	public void OnCollisionEnter(Collision collision)
	{
		Debug.Log (collision.gameObject.name);

			var phish = GameObject.FindGameObjectsWithTag("Player");
			GameObject.Destroy(collision.gameObject);
	}
}

